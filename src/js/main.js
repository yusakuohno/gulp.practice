import {commons} from './common';
import {demos} from './demo';

$(() => {
    commons();
    demos();
});
